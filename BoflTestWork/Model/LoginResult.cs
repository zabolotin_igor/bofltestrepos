﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoflTestWork.Model
{
    public class LoginResult
    {
        public string status { get; set; }
        public bool reactivated { get; set; }
        public bool authenticated { get; set; }
        public string user { get; set; }
    }
}
