﻿using BoflTestWork.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BoflTestWork.Common
{
    public class InstagramClient
    {
        private CookieContainer mCoockieC;

        private bool mLoggedIn;

        public string Username { get; set; }

        public string Password { get; set; }

        public InstagramClient() : this(string.Empty, string.Empty)
        {

        }

        public InstagramClient(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentException("username must not empty or null");

            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentException("password must not empty or null");

            Username = username;
            Password = password;

            mCoockieC = new CookieContainer();
        }


        public LoginResult LogIn()
        {
            try
            {
                var bootstrapRequest = HttpRequestBuilder.Get("https://www.instagram.com/accounts/login/", mCoockieC);
                bootstrapRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
                bootstrapRequest.Headers["Upgrade-Insecure-Requests"] = "1";
                using (var bootstrapResponse = bootstrapRequest.GetResponse() as HttpWebResponse)
                {
                    mCoockieC.Add(bootstrapResponse.Cookies);
                }
            }
            catch (Exception bex)
            {
                Debug.WriteLine("Bootstrap progress meet exception " + bex.Message);
                throw bex;
            }

            try
            {
                var data = $"username={Username}&password={Password}";
                var content = Encoding.ASCII.GetBytes(data);

                var request = HttpRequestBuilder.Post("https://www.instagram.com/accounts/login/ajax/", mCoockieC);
                request.Referer = "https://www.instagram.com/accounts/login/";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = content.Length;
                request.KeepAlive = true;
                request.Headers["Origin"] = "https://www.instagram.com";
                request.Headers["X-CSRFToken"] = mCoockieC.GetCookies(new Uri("https://www.instagram.com"))["csrftoken"].Value;
                request.Headers["X-Instagram-AJAX"] = "1";
                request.Headers["X-Requested-With"] = "XMLHttpRequest";

                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(content, 0, content.Length);
                    using (var response = request.GetResponse() as HttpWebResponse)
                    using (var responsStream = response.GetResponseStream())
                    using (var streamReader = new StreamReader(responsStream))
                    {
                        var responseData = streamReader.ReadToEnd();

                        return JsonConvert.DeserializeObject<LoginResult>(responseData);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Login progress occur exception " + ex.Message);
                throw ex;
            }
        }

        private ProfileResult GetProfile()
        {
            Debug.WriteLine("Get profile");
            var request = HttpRequestBuilder.Get("https://www.instagram.com/accounts/edit/?__a=1", mCoockieC);
            request.Referer = $"https://www.instagram.com/{Username}/";
            request.Headers["X-Requested-With"] = "XMLHttpRequest";
            request.AllowAutoRedirect = false;
            using (var response = request.GetResponse() as HttpWebResponse)
            {
                mCoockieC.Add(response.Cookies);
                using (var responseStream = response.GetResponseStream())
                using (var gzipStream = new GZipStream(responseStream, CompressionMode.Decompress))
                using (var streamReader = new StreamReader(gzipStream))
                {
                    var data = streamReader.ReadToEnd();
                    return JsonConvert.DeserializeObject<ProfileResult>(data);
                }
            }
        }

        //public void GetProfileMedia()
        //{

        //    var data = GetPublicInfo().graphql.user.edge_owner_to_timeline_media;


        //    foreach (var item in data.edges)
        //    {

        //        MessageBox.Show(item.node.display_url);

        //    }

        //}

        public RootObject GetPublicInfo()
        {
            var request = HttpRequestBuilder.Get($"https://www.instagram.com/{Username}/?__a=1", mCoockieC);
            request.Referer = $"https://www.instagram.com/{Username}/";
            request.Headers["X-Requested-With"] = "XMLHttpRequest";
            request.AllowAutoRedirect = false;
            using (var response = request.GetResponse() as HttpWebResponse)
            {
                mCoockieC.Add(response.Cookies);
                using (var responseStream = response.GetResponseStream())
                using (var gzipStream = new GZipStream(responseStream, CompressionMode.Decompress))
                using (var streamReader = new StreamReader(gzipStream))
                {
                    var data = streamReader.ReadToEnd();
                    Console.WriteLine();
                    return JsonConvert.DeserializeObject<RootObject>(data);
                }
            }


        }



    }
}
