﻿using BoflTestWork.Common;
using BoflTestWork.Model;
using BoflTestWork.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BoflTestWork.ViewModel
{
    public class MainWindowViewModel : IMainWindowViewModel, INotifyPropertyChanged
    {
        private IMainWindow View;

        public MainWindowViewModel(IMainWindow view)
        {
            this.View = view;

            this.View.SetDataContex(this);
        }
        #region Properties
        private string login;
        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;

                OnChanged("Login");
            }
        }

        private string password;
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;

                OnChanged("Password");
            }
        }

        public ObservableCollection<Node> nodesCollec { get; set; } =
             new ObservableCollection<Node>();
        #endregion


        private ICommand loginUser;


        public ICommand LoginUser
        {
            get
            {
                if (this.loginUser is null)
                {
                    this.loginUser = new RelayCommand(
                        (param) => {

                            var user = new InstagramClient(login, password);
                            var loginResult = user.LogIn();
                            if (loginResult.authenticated)
                            {
                              
                                var data=user.GetPublicInfo().graphql.user.edge_owner_to_timeline_media.edges;
                                
                                foreach (var item in data)
                                {
                                    nodesCollec.Add(item.node);
                                }
                            }

                        }, null
                    );
                }
                return this.loginUser;
            }
        }

        IMainWindow IMainWindowViewModel.View => this.View;

        protected void OnChanged([CallerMemberName]string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
