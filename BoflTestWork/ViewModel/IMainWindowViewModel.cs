﻿using BoflTestWork.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BoflTestWork.ViewModel
{
    interface IMainWindowViewModel
    {
        ICommand LoginUser { get; }

        IMainWindow View { get; }
    }
}
