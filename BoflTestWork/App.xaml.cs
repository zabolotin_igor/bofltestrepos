﻿using Autofac;
using BoflTestWork.View;
using BoflTestWork.ViewModel;
using System.Windows;

namespace BoflTestWork
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public static IContainer container { get; private set; }

        public App()
        {


            var builder = new ContainerBuilder();

           
            builder.RegisterType<MainWindow>().As<IMainWindow>().SingleInstance();
            builder.RegisterType<MainWindowViewModel>().As<IMainWindowViewModel>().SingleInstance();
            container = builder.Build();

        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var viewModel = container.Resolve<IMainWindowViewModel>();
            var mainView = viewModel.View as Window;
            this.MainWindow = mainView;
            this.MainWindow.Show();
        }
    }
}
