﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoflTestWork.View
{
    public interface IMainWindow
    {
        void SetDataContex(object context);
        bool? ShowDialog();
    }
}
